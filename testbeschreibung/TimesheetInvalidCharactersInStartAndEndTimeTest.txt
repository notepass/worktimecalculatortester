Testfall-ID: TimesheetInvalidCharactersInStartAndEndTimeTest

Zusammenfassung:
  - Testobjekt: Webanwendung: Arbeitszeitberechnung
  - Ziel: Es wird versucht invalide Zeichen in verschiedene Eingabefelder einzugeben. Diese sollten abgelehnt werden.

Setup:
  - Systemzustand:
    - Die Seite sollte in "frisch" gestarteten Browser geöffnet werden
    - Ein Webserver, welcher die Seite ausliefert muss im Netzwerk vorhanden sein, um einen einheitlichen Datenstand zu gewährleisten
  - Vorläufige Aktivitäten:
    - Vor der Testausführung ist der Browser neu zu starten

Testschritte:
  1.
    a. Testschritt: Öffnen der Webseite (z.B. http://127.0.0.1/demo.html)
    b. Erwartetes Ergebnis: Eine Webseite öffnet sich
  2.
    a. Testschritt: Falls Vorhanden den Inhalt der Felder "Beginn" und "Ende" Löschen
    b. Erwartetes Ergebnis: Die Felder "Beginn" und "Ende" sind leer
  3.
    a. Testschritt: Im Feld "Beginn" den Wert "test" eintragen
    b. Erwartetes Ergebnis: Im Feld "Beginn" ist kein Wert eingetragen (Invalide Zeichen sollten nicht eintragbar sein)
    c. Testdaten: Wert "test"
  4.
    a. Testschritt: Im Feld "Ende" den Wert "test" eintragen
    b. Erwartetes Ergebnis: Erwartetes Ergebnis: Im Feld "Ende" ist kein Wert eingetragen (Invalide Zeichen sollten nicht eintragbar sein)
    c. Testdaten: Wert "test"

Priorität: Mittel => Invalide Eingaben sollten im Backend zu einem Fehler führen und damit kein Problem darstellen. Allerdings könnte dies verwirrend für Nutzer sein.

Ergebnis: Siehe Testreport