package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests.util;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;

import java.util.Map;

public class TestUtils {
    public static void assertCustomTimeDropDownContains(String[] entry, TimesheetPage page) {
        Map<String, String> options = page.getCustomWorkTimePossibilities();
        Assert.assertTrue(
                "Expected value {label=\"" + entry[0] + "\", value=\"" + entry[1] + "\"} " +
                        "not found in dropdown for custom break time. " +
                        "This means this test needs to be fixed.",
                options.containsKey(entry[0])
        );
    }
}
