package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic;

import org.openqa.selenium.WebElement;

/**
 * Wrapper element for a [button]-element
 */
public class HtmlButton extends HtmlElement {
    public HtmlButton(WebElement browserElement) {
        super(browserElement);
    }

    /**
     * Clicks the button
     */
    public void click() {
        browserElement.click();
    }
}
