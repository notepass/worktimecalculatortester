package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;

public class TimesheetInvalidCharactersInStartAndEndTimeTest extends SeleniumTestBase {
    public TimesheetInvalidCharactersInStartAndEndTimeTest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    /**
     * Tries to input text characters into the start/end field
     */
    @Test
    public void invalidCharactersInStartAndEndTimeTest() {
        TimesheetPage timesheetPage = new TimesheetPage(this);
        String text = "test";
        timesheetPage.fillOutForm(text, text);

        // Guessed
        String allowedContentRegex = "((1[0-9]{1}|2[0-4]{1}|0)|[0-5]{1}[0-9]{1}):[0-5]{1}[0-9]{1}";

        String startTimeFieldContent = timesheetPage.getStartTimeField().getContent();
        String endTimeFieldContent = timesheetPage.getEndTimeField().getContent();

        if (startTimeFieldContent.isEmpty() && endTimeFieldContent.isEmpty()) {
            // No data in fields, test passed, as no valid characters were entered in the first place
            return;
        }

        if (startTimeFieldContent.matches(allowedContentRegex) && endTimeFieldContent.matches(allowedContentRegex)) {
            // Some default (or interpolated) time values were entered into the fields - Test passed
            return;
        }

        Assert.assertTrue(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "The \"Beginn\" input field should not allow entering invalid characters",
                        "The \"Beginn\" input field contains invalid characters (Value: \"" + startTimeFieldContent + "\")",
                        capabilities.getClass().getSimpleName()
                )
                , startTimeFieldContent.matches(allowedContentRegex)
        );

        Assert.assertTrue(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "The \"Ende\" input field should not allow entering invalid characters",
                        "The \"Ende\" input field contains invalid characters (Value: \"" + endTimeFieldContent + "\")",
                        capabilities.getClass().getSimpleName()
                )
                , startTimeFieldContent.matches(allowedContentRegex)
        );
    }
}
