package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.util;

public class ThreadUtils {
    public static Thread startThreadDelayed(Runnable r, int startDelayMs) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(startDelayMs);
                } catch (InterruptedException e) {
                    // Ignore
                }
                r.run();
            }
        });
        t.start();
        return t;
    }
}
