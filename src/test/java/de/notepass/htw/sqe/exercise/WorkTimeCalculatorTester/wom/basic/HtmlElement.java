package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic;

import org.openqa.selenium.WebElement;

public abstract class HtmlElement {
    protected WebElement browserElement;

    public HtmlElement(WebElement browserElement) {
        this.browserElement = browserElement;
    }

    public WebElement getBrowserElement() {
        return browserElement;
    }
}
