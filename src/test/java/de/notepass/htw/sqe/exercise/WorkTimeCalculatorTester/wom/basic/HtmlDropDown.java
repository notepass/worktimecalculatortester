package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.*;

public class HtmlDropDown extends HtmlElement {
    private Select dropdown;

    public HtmlDropDown(WebElement browserElement) {
        super(browserElement);
        this.dropdown = new Select(browserElement);
    }

    /**
     * Returns the values possible for selection
     * @param includeDisabled If set to true, the result will also contain unselectable entries (disabled=true)
     * @return Key-Value pairs of available options. Keys are the technical value, values are the labels
     */
    public Map<String, String> getPossibleValues(boolean includeDisabled) {
        List<WebElement> elements = dropdown.getOptions();

        //Create a tree set to keep the sequence of the elements
        TreeMap<String, String> res = new TreeMap<>();
        for (WebElement element:elements) {
            String value = element.getAttribute("value");
            String readableName = element.getText();
            boolean isDisabled = element.getAttribute("disabled") != null;

            if (value == null) {
                value = readableName;
            }

            // Only add data if the element isn't disabled or adding disabled elements is allowed
            if (!isDisabled || includeDisabled) {
                res.put(value, readableName);
            }
        }

        return res;
    }

    /**
     * Tries to select the given value by the value-attribute
     * @param value The value of the value-attribute for the option to select
     * @return Key-Value pair of selected option. Keys are the technical value, values are the labels. Or null if nothing is selected
     */
    public Map<String, String> selectEntryByValue(String value) {
        dropdown.selectByValue(value);
        return getCurrentlySelectedElement();
    }

    /**
     * Tries to select the given value by the readable label (text content)
     * @param value The value of the label for the option to select
     * @return Key-Value pair of selected option. Keys are the technical value, values are the labels. Or null if nothing is selected
     */
    public Map<String, String> selectEntryByLabel(String value) {
        dropdown.selectByVisibleText(value);
        return getCurrentlySelectedElement();
    }

    /**
     * Returns the currently selected option or null if none is selected
     * @return Key-Value pair of selected option. Keys are the technical value, values are the labels. Or null if nothing is selected
     */
    public Map<String, String> getCurrentlySelectedElement() {
        Map<String, String> res = new HashMap<>();
        WebElement selected = dropdown.getFirstSelectedOption();
        if (selected == null) {
            return null;
        }

        res.put(selected.getAttribute("value"), selected.getAttribute("innerText"));

        return res;
    }
}
