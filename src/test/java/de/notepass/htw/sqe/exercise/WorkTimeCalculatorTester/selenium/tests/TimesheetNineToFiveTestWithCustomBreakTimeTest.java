package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests.util.TestUtils;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;

import java.util.Map;

public class TimesheetNineToFiveTestWithCustomBreakTimeTest extends SeleniumTestBase {
    public TimesheetNineToFiveTestWithCustomBreakTimeTest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    /**
     * Test to check a simple use case - A person working from 9:00 to 17:00 with a custom break time of 1.5 hours
     */
    @Test
    public void nineToFiveTestWithCustomBreakTime() {
        TimesheetPage timesheetPage = new TimesheetPage(this);
        String expectedResult = "6.5h";
        String[] neededDropdownValue = {"1.5", "90m"};
        TestUtils.assertCustomTimeDropDownContains(neededDropdownValue, timesheetPage);
        String actualResult = timesheetPage.fillOutFormAndCalculateWorkTime("9:00", "17:00", false, neededDropdownValue[0]);

        Assert.assertEquals(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "When a work time greater than \"" + neededDropdownValue[0] + "h\" is entered, and a custom break time of \"" + neededDropdownValue[1] + "\" is selected the calculated work time should be \"" + expectedResult + "\"",
                        "A calculated work time of \"" + actualResult + "\"",
                        capabilities.getClass().getSimpleName()
                ),
                expectedResult,
                actualResult
        );
    }
}
