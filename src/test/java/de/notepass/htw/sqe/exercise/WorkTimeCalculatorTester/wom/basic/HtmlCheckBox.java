package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic;

import org.openqa.selenium.WebElement;

public class HtmlCheckBox extends HtmlElement {
    private WebElement labelElement = null;

    public HtmlCheckBox(WebElement browserElement, WebElement labelElement) {
        super(browserElement);
        this.labelElement = labelElement;
    }

    public HtmlCheckBox(WebElement browserElement) {
        super(browserElement);
    }

    /**
     * Tries to check the checkbox if not already checked
     * @return State after trying to check to checkbox (true = checked; false = unchecked)
     */
    public boolean check() {
        if (isCurrentlyChecked()) {
            //Don't do anything. Checkbox is checked
            return true;
        } else {
            // Click to (hopefully) check
            click();
            return isCurrentlyChecked();
        }
    }

    /**
     * Tries to uncheck the checkbox if not already unchecked
     * @return If checkbox could be unchecked <b style="color: red">(true = unchecked; false = checked)</b>
     */
    public boolean uncheck() {
        if (isCurrentlyChecked()) {
            // Click to (hopefully) uncheck
            click();
            return !isCurrentlyChecked();
        } else {
            //Don't do anything. Checkbox is unchecked
            return true;
        }
    }

    /**
     * Returns if the checkbox is currently checked
     * @return true = checked, false = unchecked
     */
    public boolean isCurrentlyChecked() {
        return browserElement.isSelected();
    }

    /**
     * Clicks the checkbox
     */
    public void click() {
        if (labelElement != null && labelElement.isDisplayed() && !browserElement.isDisplayed()) {
            labelElement.click();
        } else {
            browserElement.click();
        }
    }
}
