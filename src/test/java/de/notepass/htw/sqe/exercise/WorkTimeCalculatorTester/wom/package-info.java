/**
 * This package contains data for the "webpage to object" mapping data structure. This allows
 * for better code readability and easier modification/extension as it avoids duplicates.
 */
package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom;