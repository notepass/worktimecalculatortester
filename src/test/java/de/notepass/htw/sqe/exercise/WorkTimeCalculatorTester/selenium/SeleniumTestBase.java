package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Base class for selenium tests containing some utility functions (This is also a wrapper for the WebDriver class used
 * in many woms)
 */
@RunWith(Parameterized.class)
public class SeleniumTestBase {
    public static enum REQUESTED_BROWSER_MAPPING {
        IE("IE", new InternetExplorerOptions()),
        EDGE("Edge", new EdgeOptions()),
        FIREFOX("Firefox", new FirefoxOptions()),
        CHROME("Chrome", new ChromeOptions()),
        OPERA("Opera", new OperaOptions()),
        SAFARI("Safari", new SafariOptions());

        private String readableName;
        private MutableCapabilities capabilities;

        REQUESTED_BROWSER_MAPPING(String readableName, MutableCapabilities capabilities) {
            this.readableName = readableName;
            this.capabilities = capabilities;
        }

        public String getReadableName() {
            return readableName;
        }

        public MutableCapabilities getCapabilities() {
            return capabilities;
        }

        public static REQUESTED_BROWSER_MAPPING getFromReadableName(String readableName) {
            for (REQUESTED_BROWSER_MAPPING rm : values()) {
                if (rm.getReadableName().equalsIgnoreCase(readableName)) {
                    return rm;
                }
            }

            return null;
        }
    }

    public static final int DEFAULT_IMPLICIT_TIMEOUT_MS = 3000;
    public static final String ERROR_MESSAGE_TEMPLATE =
            "Test failed." + System.lineSeparator() +
                    "Expected: %s." + System.lineSeparator() +
                    "Got: %s. BrowserCapabilities: %s";

    public static final String ERROR_MESSAGE_TEMPLATE_CUSTOM =
            "Test failed." + System.lineSeparator() +
                    "%s." + System.lineSeparator() +
                    "BrowserCapabilities: %s";

    protected static ThreadLocal driver = new ThreadLocal<>();

    public WebDriver getDriver() {
        return (WebDriver) driver.get();
    }

    public MutableCapabilities capabilities;

    /**
     * Allows accessing the findElementsBy-method with a temporary, custom, timeout
     *
     * @param finder
     * @param timeoutMS
     * @return
     */
    public List<WebElement> getElementsBy(By finder, int timeoutMS) {
        getDriver().manage().timeouts().implicitlyWait(timeoutMS, TimeUnit.MILLISECONDS);
        List<WebElement> elements = getDriver().findElements(finder);
        getDriver().manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
        return elements;
    }

    public SeleniumTestBase(MutableCapabilities capabilities) {
        this.capabilities = capabilities;
    }

    @Before
    public void setUp() throws Exception {
        RemoteWebDriver webDriver = new RemoteWebDriver(new URL("http://" + getSeleniumHost() + ":" + getSeleniumPort() + "/wd/hub"), capabilities);
        webDriver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
        driver.set(webDriver);
        webDriver.navigate().to(getUrlForPageToTest());
    }

    @After
    public void tearDown() {
        getDriver().quit();
    }

    @AfterClass
    public static void remove() {
        driver.remove();
    }

    public static String getSeleniumHost() {
        String host = System.getenv("SELENIUM_HUB_HOST");
        if (host == null || host.isEmpty()) {
            host = "localhost";
        }

        return host;
    }

    public static int getSeleniumPort() {
        String port = System.getenv("SELENIUM_HUB_PORT");
        if (port == null || port.isEmpty()) {
            port = "4444";
        }

        return Integer.parseInt(port);
    }

    public static String getUrlForPageToTest() {
        String host = System.getenv("TEST_PAGE_URL");
        if (host == null || host.isEmpty()) {
            host = "http://localhost:80/demo.html";
        }

        return host;
    }

    @Parameterized.Parameters
    public static MutableCapabilities[] getRequestedCapabilities() {
        String requestedTestBrowsers = System.getenv("SELENIUM_BROWSER_LIST");

        if (requestedTestBrowsers == null || requestedTestBrowsers.isEmpty()) {
            return new MutableCapabilities[]{new FirefoxOptions(), new ChromeOptions()};
        }

        String[] browserList = requestedTestBrowsers.split(Pattern.quote(";"));
        List<MutableCapabilities> result = new ArrayList<>(browserList.length);
        String validBrowserValues = Arrays.toString(
                Arrays
                        .stream(REQUESTED_BROWSER_MAPPING.values())
                        .map(REQUESTED_BROWSER_MAPPING::getReadableName)
                        .toArray(String[]::new)
        );

        Assert.assertNotEquals(
                "Invalid SELENIUM_BROWSER_LIST environment variable given. Variable should contain a " +
                "semicolon-separated list containing one or more of the following entries: "
                + validBrowserValues, 0, browserList.length
        );

        for (String browser : browserList) {
            REQUESTED_BROWSER_MAPPING rm = REQUESTED_BROWSER_MAPPING.getFromReadableName(browser);

            Assert.assertNotNull(
                    "Unknown browser \"" + browser + "\" passed in SELENIUM_BROWSER_LIST environment variable." +
                            "Valid browsers are: " + validBrowserValues,
                    rm
            );

            result.add(rm.getCapabilities());
        }

        return result.toArray(new MutableCapabilities[0]);
    }
}
