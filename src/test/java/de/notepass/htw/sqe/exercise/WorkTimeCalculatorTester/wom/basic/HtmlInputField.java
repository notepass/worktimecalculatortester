package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic;

import org.openqa.selenium.WebElement;

/**
 * Wrapper-class for html-elements of type "input". For practical reasons there is no subdivision in type like
 * "HtmlTextInputField" and the like
 */
public class HtmlInputField extends HtmlElement {
    public HtmlInputField(WebElement browserElement) {
        super(browserElement);
    }

    /**
     * Sends keystrokes for the given text to the field. Please note: This will not work for [input type=submit]!
     * @param value Text to write
     * @return Text content of the field after the text was written, or null if element doesn't support text input
     */
    public String setContentValue(String value) {
        if ("submit".equalsIgnoreCase(browserElement.getAttribute("type"))) {
            return null;
        }

        browserElement.sendKeys(value);
        return browserElement.getText();
    }

    /**
     * Removes the currently entered content from the input field
     */
    public void clearContent() {
        browserElement.clear();
    }

    /**
     * Gets the current content of the input field
     * @return
     */
    public String getContent() {
        return browserElement.getAttribute("value");
    }
}
