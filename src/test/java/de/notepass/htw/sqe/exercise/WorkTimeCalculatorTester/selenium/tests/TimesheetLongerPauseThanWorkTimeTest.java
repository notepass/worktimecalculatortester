package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests.util.TestUtils;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;

public class TimesheetLongerPauseThanWorkTimeTest extends SeleniumTestBase {
    public TimesheetLongerPauseThanWorkTimeTest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    /**
     * Tests what happens if the break time is longer than the work time - Granted this isn't all too realistic but still
     */
    @Test
    public void longerPauseThanWorkTimeTest() {
        TimesheetPage timesheetPage = new TimesheetPage(this);

        String[] neededDropdownValue = {"1.5", "90m"};
        TestUtils.assertCustomTimeDropDownContains(neededDropdownValue, timesheetPage);

        String expectedResult = "0h";
        String actualResult = timesheetPage.fillOutFormAndCalculateWorkTime("9:00", "10:00", false, neededDropdownValue[0]);

        Assert.assertEquals(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "When a break time longer than the work time is entered, a work time of \"" + expectedResult + "\" is the calculated result",
                        "A calculated work time of \"" + actualResult + "\"",
                        capabilities.getClass().getSimpleName()
                )
                , expectedResult
                , actualResult
        );
    }
}
