package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;

public class TimesheetEndAfterBeginTimeTest extends SeleniumTestBase {
    public TimesheetEndAfterBeginTimeTest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    /**
     * Sets the start time to earlier than the end time - signaling tht the work went on over midnight and into a new day
     */
    @Test
    public void endAfterBeginTimeTest() {
        TimesheetPage timesheetPage = new TimesheetPage(this);
        String expectedResult = "1.5h";
        String actualResult = timesheetPage.fillOutFormAndCalculateWorkTime("23:00", "1:00");

        Assert.assertEquals(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "A calculated work time of \""+expectedResult+"\", as the work time spanned over the end of a day and into a new one",
                        "A calculated work time of \""+actualResult+"\"",
                        capabilities.getClass().getSimpleName()
                )
                , expectedResult
                , actualResult
        );
    }

}
