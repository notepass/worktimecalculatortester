package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.util.ThreadUtils;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic.HtmlButton;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic.HtmlCheckBox;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic.HtmlDropDown;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.basic.HtmlInputField;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Map;

/**
 * Wrapper-object for the webpage to test ("Arbeitszeit")
 */
public class TimesheetPage {
    private HtmlInputField startTimeField;
    private HtmlInputField endTimeField;
    private HtmlButton calculateButton;
    private HtmlCheckBox defaultBreakTimeCheckbox;
    private WebElement calculatedWorkTime;
    private HtmlDropDown nonStandardBreakTimeDropDown;
    private WebElement loadingSpinner;

    private SeleniumTestBase seleniumConnector;
    private int maxProcessingWaitTime = 10;

    /**
     * Creates a self-configured object instance for working with the currently loaded webpage
     * @param seleniumConnector
     */
    public TimesheetPage(SeleniumTestBase seleniumConnector) {
        this.seleniumConnector = seleniumConnector;

        startTimeField = new HtmlInputField(seleniumConnector.getDriver().findElement(By.id("input_begin")));
        endTimeField = new HtmlInputField(seleniumConnector.getDriver().findElement(By.id("input_end")));
        calculateButton = new HtmlButton(seleniumConnector.getDriver().findElement(By.id("btn_calculate")));
        defaultBreakTimeCheckbox = new HtmlCheckBox(seleniumConnector.getDriver().findElement(By.id("default_break_duration")), seleniumConnector.getDriver().findElement(By.xpath("//label[@for = 'default_break_duration'][1]")));
        calculatedWorkTime = seleniumConnector.getDriver().findElement(By.id("result_container"));
        nonStandardBreakTimeDropDown = new HtmlDropDown(seleniumConnector.getDriver().findElement(By.id("custom_break_duration")));
        loadingSpinner = seleniumConnector.getDriver().findElement(By.id("loading_spinner"));
    }

    /**
     * Fills out the form given on the web page and returns the value of the "Dauer" output
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String calculateWorkTime(String startTime, String endTime) {
        return fillOutFormAndCalculateWorkTime(startTime, endTime, true, null);
    }

    /**
     * Fills out the form given on the web page and returns the value of the "Dauer" output
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @param customBreakTimeValue Value for custom break time dropdown (This will disble the "Standard Pause (30m)" selection
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String calculateWorkTime(String startTime, String endTime, String customBreakTimeValue) {
        return fillOutFormAndCalculateWorkTime(startTime, endTime, false, customBreakTimeValue);
    }

    /**
     * Fills out the form on the website and submits it. It will then wait until processing finishes and return the calculated "Arbeitszeit".<br>
     * If a value is passed as "null", the according field will not be touched. <b>If you want to clear a field,
     * please pass the according value instad of null</b>
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String fillOutFormAndCalculateWorkTime(String startTime, String endTime) {
        return fillOutFormAndCalculateWorkTime(startTime, endTime, true, null, maxProcessingWaitTime);
    }

    /**
     * Fills out the form on the website and submits it. It will then wait until processing finishes and return the calculated "Arbeitszeit".<br>
     * If a value is passed as "null", the according field will not be touched. <b>If you want to clear a field,
     * please pass the according value instad of null</b>
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @param standardBreakTimeChecked Value for "Standard Pause (30m)" checkbox
     * @param customBreakTimeValue Value for custom break time dropdown
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String fillOutFormAndCalculateWorkTime(String startTime, String endTime, Boolean standardBreakTimeChecked, String customBreakTimeValue) {
        return fillOutFormAndCalculateWorkTime(startTime, endTime, standardBreakTimeChecked, customBreakTimeValue, maxProcessingWaitTime);
    }

    /**
     * Fills out the form on the website and submits it. It will then wait until processing finishes and return the calculated "Arbeitszeit".<br>
     * If a value is passed as "null", the according field will not be touched. <b>If you want to clear a field,
     * please pass the according value instad of null</b><br>
     * If the processing time exceeds the value given in maxProcessingWaitTimeSeconds, an exception will be thrown
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @param standardBreakTimeChecked Value for "Standard Pause (30m)" checkbox
     * @param customBreakTimeValue Value for custom break time dropdown
     * @param maxProcessingWaitTimeSeconds Maximum wait time (in seconds) for the processing to finish
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String fillOutFormAndCalculateWorkTime(String startTime, String endTime, Boolean standardBreakTimeChecked, String customBreakTimeValue, int maxProcessingWaitTimeSeconds) {
        fillOutForm(startTime, endTime, standardBreakTimeChecked, customBreakTimeValue);
        submitFormAndWaitForProcessingToFinish(maxProcessingWaitTimeSeconds);
        return getCalculatedWorkTime();
    }

    /**
     * Fills out the form on the website.<br>
     * If a value is passed as "null", the according field will not be touched. <b>If you want to clear a field,
     * please pass the according value instad (Empty string) of null</b>
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     */
    public void fillOutForm(String startTime, String endTime) {
        fillOutForm(startTime, endTime, true, null);
    }

    /**
     * Fills out the form on the website.<br>
     * If a value is passed as "null", the according field will not be touched. <b>If you want to clear a field,
     * please pass the according value instad (Empty string) of null</b>
     * @param startTime Value for the field "Beginn"
     * @param endTime Value for the field "Ende"
     * @param standardBreakTimeChecked Value for "Standard Pause (30m)" checkbox
     * @param customBreakTimeValue Value for custom break time dropdown
     */
    public void fillOutForm(String startTime, String endTime, Boolean standardBreakTimeChecked, String customBreakTimeValue) {
        if (startTime != null) {
            startTimeField.clearContent();
            startTimeField.setContentValue(startTime);
        }

        if (endTime != null) {
            endTimeField.clearContent();
            endTimeField.setContentValue(endTime);
        }

        if (standardBreakTimeChecked != null) {
            if (standardBreakTimeChecked) {
                boolean isReallyChecked = defaultBreakTimeCheckbox.check();

                Assert.assertTrue("Checkbox \"Standard Pause (30m)\" could not be checked as requested by code",
                        isReallyChecked);
            } else {
                boolean isReallyUnchecked = defaultBreakTimeCheckbox.uncheck();

                Assert.assertTrue("Checkbox \"Standard Pause (30m)\" could not be unchecked as requested by code",
                        isReallyUnchecked);
            }
        }

        if (customBreakTimeValue != null) {
            Assert.assertTrue(
                    "Given custom break time is not a selectable value. Use TimesheetPage#getCustomWorkTimePossibilities()" +
                            "to obtain a list of possible values and pass the key of the wanted time into this function.",
                    nonStandardBreakTimeDropDown.getPossibleValues(false).containsKey(customBreakTimeValue)
            );

            Map<String, String> selectedValue = nonStandardBreakTimeDropDown.selectEntryByValue(customBreakTimeValue);

            Assert.assertTrue("Could not select value \"" + customBreakTimeValue + "\" for custom breakl time " +
                            "selection dropdown, as requested by code",
                    selectedValue.containsKey(customBreakTimeValue)
            );
        }
    }

    /**
     * Presses the "Dauer berechnen ..." button and waits until the loading spinner appears and disappears again (= Waiting for processing to finish)
     */
    public void submitFormAndWaitForProcessingToFinish() {
        submitFormAndWaitForProcessingToFinish(maxProcessingWaitTime);
    }

    /**
     * Presses the "Dauer berechnen ..." button and waits until the loading spinner appears and disappears again
     * (= Waiting for processing to finish)<br>
     * If the processing doesn't finish after maxWaitTimeInSeconds seconds, an exception will be thrown
     */
    public void submitFormAndWaitForProcessingToFinish(int maxWaitTimeInSeconds) {
        String oldValue = getCalculatedWorkTime();
        int pollingDelayMs = 100;
        //calculateButton.click();

        try {
            Wait wait = new FluentWait(seleniumConnector.getDriver())
                    .withTimeout(Duration.ofSeconds(maxWaitTimeInSeconds))
                    //Custom check interval of 100ms, as 500ms is just way too long and leads to flakiness
                    .pollingEvery(Duration.ofMillis(pollingDelayMs));

            // Click button after starting to wait. Clicking the button before sometimes lead to selenium starting to check
            // the spinner after it already disappeared again
            // TODO: YAK!
            ThreadUtils.startThreadDelayed(() -> calculateButton.click(), pollingDelayMs);

            // Wait until the loading icon becomes visible, it should appear directly (except if selenium "oversleeps" again)
            wait.until(ExpectedConditions.visibilityOf(loadingSpinner));

            wait = new WebDriverWait(seleniumConnector.getDriver(), maxWaitTimeInSeconds);

            // Then wait again until it vanishes (For the defined time)
            wait.until(ExpectedConditions.invisibilityOf(loadingSpinner));
        } catch (Exception e) {
            // Trying to recover from selenium sleepiness: If the displayed value changed, processing was done but selenium
            // just didn't realize the spinner changing (This is the bane of all my selenium tests)

            String newValue = getCalculatedWorkTime();

            if (newValue.equals(oldValue)) {
                throw e;
            }
        }
    }

    /**
     * Returns the "Arbeitszeit"-value outputted under "Dauer"
     * @return "Arbeitszeit"-value outputted under "Dauer"
     */
    public String getCalculatedWorkTime() {
        return calculatedWorkTime.getText();
    }

    /**
     * Returns the possible options which can be selected in the custom break time dropdown
     * @return Key-Value pairs of available options. Keys are the technical value, values are the labels
     */
    public Map<String, String> getCustomWorkTimePossibilities() {
        return nonStandardBreakTimeDropDown.getPossibleValues(false);
    }

    /**
     * Returns the default max wait time for processing the work time. This time is used when no explicit timing is given
     * @return The default max wait time for processing the work time
     */
    public int getMaxProcessingWaitTime() {
        return maxProcessingWaitTime;
    }

    /**
     * Sets the default max wait time for processing the work time. This time is used when no explicit timing is given
     * @param maxProcessingWaitTime The default max wait time for processing the work time
     */
    public void setMaxProcessingWaitTime(int maxProcessingWaitTime) {
        this.maxProcessingWaitTime = maxProcessingWaitTime;
    }

    public HtmlInputField getStartTimeField() {
        return startTimeField;
    }

    public HtmlInputField getEndTimeField() {
        return endTimeField;
    }

    public HtmlButton getCalculateButton() {
        return calculateButton;
    }

    public HtmlCheckBox getDefaultBreakTimeCheckbox() {
        return defaultBreakTimeCheckbox;
    }

    public HtmlDropDown getNonStandardBreakTimeDropDown() {
        return nonStandardBreakTimeDropDown;
    }

    public WebElement getLoadingSpinner() {
        return loadingSpinner;
    }

    public SeleniumTestBase getSeleniumConnector() {
        return seleniumConnector;
    }
}
