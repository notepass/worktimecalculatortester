package de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.tests;

import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.selenium.SeleniumTestBase;
import de.notepass.htw.sqe.exercise.WorkTimeCalculatorTester.wom.TimesheetPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;

public class TimesheetNineToFiveTestWithDefaultBreakTimeTest extends SeleniumTestBase {
    public TimesheetNineToFiveTestWithDefaultBreakTimeTest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    /**
     * Test to check a simple use case - A person working from 9:00 to 17:00 with a default break time
     */
    @Test
    public void nineToFiveTestWithDefaultBreakTime() {
        TimesheetPage timesheetPage = new TimesheetPage(this);
        String expectedResult = "7.5h";
        String actualResult = timesheetPage.fillOutFormAndCalculateWorkTime("9:00", "17:00");
        Assert.assertEquals(
                String.format(
                        ERROR_MESSAGE_TEMPLATE,
                        "A calculated work time of \""+expectedResult+"\"",
                        "A calculated work time of \""+actualResult+"\"",
                        capabilities.getClass().getSimpleName()
                )
                ,expectedResult
                , actualResult
        );
    }
}
